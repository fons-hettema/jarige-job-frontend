/*--------------------------------------------------------------
 [Us Media] Gulpfile for WordPress
 - add "return" before gulp.src to asynchronize tasks
 - Make sure your WP theme const is correct
 --------------------------------------------------------------*/

/*--------------------------------------------------------------
 Declarations
 --------------------------------------------------------------*/
const gulp = require("gulp");
const gulpsync = require('gulp-sync')(gulp);
const del = require("del");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const rename = require("gulp-rename");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const browsersync = require("browser-sync");
const run = require('gulp-run');
const newer = require('gulp-newer');
const image = require("gulp-image");
const svgstore = require("gulp-svgstore");
const svgmin = require("gulp-svgmin");
const sassLint = require("gulp-sass-lint");
const jslint = require("gulp-jslint");
const sourcemaps = require("gulp-sourcemaps");
const babel = require("gulp-babel");

const reload = browsersync.reload;

const srcPath = {
    "src": "./src/",
    "html": "./src/html/",
    "styles": "./src/styles/",
    "scripts": "./src/scripts/",
    "images": "./src/static/images/",
    "icons": "./src/static/svg/icons/",
    "logos": "./src/static/svg/logos/",
    "static": "./src/static/",
    "vendor": "./node_modules/"
};

const targetPath = {
    "assets": "./dist/assets/",
    "styles": "./dist/assets/css/",
    "scripts": "./dist/assets/scripts/",
    "images": "./dist/assets/images/",
    "app": "./app/",
};


/*--------------------------------------------------------------
 Main Gulp tasks
 --------------------------------------------------------------*/
// Default - in sync
gulp.task("default", gulpsync.sync(["clean", "styles", "scripts", "copy", "browsersync", "watch"]));

// Build
gulp.task("build", ["build:dev"]);

// Clean
gulp.task("clean", ["clean:dist", "clean:app"]);

// Copy - in sync
gulp.task("copy", gulpsync.sync(["copy:static", "copy:jquery", "copy:vendor", "copy:assets", "copy:html"]));

// Lint
gulp.task("lint", ["lint:js", "lint:sass"]);

// Shell
gulp.task("shell", ["shell:npm"]);

// Optimize
gulp.task("optimize", ["optimize:svgIcons", "optimize:svgLogos"]);


/*--------------------------------------------------------------
 Builders
 --------------------------------------------------------------*/
// Build the development environment
gulp.task("build:dev", gulpsync.sync(["shell", "clean", "optimize", "styles", "scripts", "copy"]));


/*--------------------------------------------------------------
 BrowserSync
 --------------------------------------------------------------*/
gulp.task("browsersync", function () {
    browsersync({
        server: {
            baseDir: "./app"
        }
    });
});


/*--------------------------------------------------------------
 Shell commands
 --------------------------------------------------------------*/
gulp.task("shell:npm", function () {
    run('npm install').exec()
});


/*--------------------------------------------------------------
 Compilers (transform, compile, compress and/or concat)
 --------------------------------------------------------------*/
// Styles
// gulp.task("styles", ["lint:sass"], function () {
//     return gulp.src(srcPath.styles + "**/*.scss")
//         .pipe(newer(targetPath.Styles + "layout.css"))
//         .pipe(sourcemaps.init())
//         .pipe(sass())
//         .pipe(autoprefixer({browsers: ["last 2 versions", "> 5%", "Firefox ESR"]}))
//         .pipe(cleanCSS({compatibility: "ie8"}))
//         .pipe(sourcemaps.write())
//         .pipe(gulp.dest(targetPath.styles))
// });

gulp.task("styles", function () {
    return gulp.src(srcPath.styles + "**/*.scss")
        .pipe(newer(targetPath.styles + "layout.css"))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({browsers: ["last 2 versions", "> 5%", "Firefox ESR"]}))
        .pipe(cleanCSS({compatibility: "ie8"}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(targetPath.styles))
});

// Scripts
gulp.task("scripts", ["lint:js"], function () {
    return gulp.src(srcPath.scripts + "**/*.js")
        .pipe(newer(targetPath.scripts + "app.js"))
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat("app.js"))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(targetPath.scripts))
});


/*--------------------------------------------------------------
 Optimizers
 --------------------------------------------------------------*/
gulp.task("optimize:svgIcons", function () {
    return gulp.src(srcPath.icons + "*.svg")
        .pipe(rename({prefix: "icon--"}))
        .pipe(svgmin(function () {
            const prefix = srcPath.icons;
            return {
                plugins: [{
                    cleanupAttrs: true
                }, {
                    removeElementsByAttr: true
                }, {
                    removeAttrs: {attrs: '(fill|fill-rule)'}
                }, {
                    removeUselessStrokeAndFill: true
                }, {
                    removeUnknownsAndDefaults: true
                }, {
                    removeDoctype: true
                }, {
                    removeComments: true
                }, {
                    removeTitle: true
                }, {
                    cleanupIDs: {
                        prefix: prefix + "-",
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest(targetPath.images));
});

gulp.task("optimize:svgLogos", function () {
    return gulp.src(srcPath.logos + "*.svg")
        .pipe(rename({prefix: "logo--"}))
        .pipe(svgmin(function () {
            const prefix = srcPath.logos;
            return {
                plugins: [{
                    cleanupAttrs: true
                }, {
                    removeElementsByAttr: true
                }, {
                    removeAttrs: false
                }, {
                    removeUselessStrokeAndFill: false
                }, {
                    removeUnknownsAndDefaults: true
                }, {
                    removeDoctype: true
                }, {
                    removeComments: true
                }, {
                    removeTitle: true
                }, {
                    cleanupIDs: {
                        prefix: prefix + "-",
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest(targetPath.images));
});

// Critical CSS
gulp.task("optimize:critcss", function (cb) { //src: http://fourkitchens.com/blog/article/use-gulp-automate-your-critical-path-css
    critical.generate({
        base: "./",
        src: "./src/index.html",
        css: ["./dist/css/style.min.css"],
        dimensions: [{
            width: 320,
            height: 480
        }, {
            width: 768,
            height: 1024
        }, {
            width: 1280,
            height: 960
        }],
        dest: targetPath.styles + "./critical.min.css",
        minify: true,
        extract: false
        //ignore: ["font-face"]
    });
});


/*--------------------------------------------------------------
 Copiers
 --------------------------------------------------------------*/
// Copy static
gulp.task("copy:static", function () {
    return gulp.src([srcPath.static + "**/*.**", "!" + srcPath.static + "/svg"])
        .pipe(gulp.dest(targetPath.assets));
});

// Copy html
gulp.task("copy:html", function () {
    return gulp.src([srcPath.html + "**/*.**"])
        .pipe(gulp.dest(targetPath.app))
        .on("end", reload);
});

// Copy assets
gulp.task("copy:assets", function () {
    return gulp.src("./dist/**")
        .pipe(gulp.dest(targetPath.app))
        .on("end", reload);
});

// Copy styles
gulp.task("copy:styles", ["styles"], function () {
    return gulp.src("./dist/**")
        .pipe(gulp.dest(targetPath.app))
        .on("end", reload);
});

// Copy scripts
gulp.task("copy:scripts", ["scripts"], function () {
    return gulp.src("./dist/**")
        .pipe(gulp.dest(targetPath.app))
         .on("end", reload);
});

// Copy JQuery
gulp.task("copy:jquery", function () {
    return gulp.src(srcPath.vendor + "jquery/dist/jquery.min.js")
        .pipe(gulp.dest(targetPath.scripts));
});

gulp.task("copy:vendor", function () {
    return gulp.src([
    ])
        .pipe(concat("vendor.js"))
        .pipe(gulp.dest(targetPath.scripts));
});



/*--------------------------------------------------------------
 Cleaners
 --------------------------------------------------------------*/
// Clean dist
gulp.task("clean:dist", function () {
    del.sync("./dist/**/*");
});

// Clean app
gulp.task("clean:app", function () {
    del.sync("./app/**/*");
});


/*--------------------------------------------------------------
 Linters
 --------------------------------------------------------------*/
// JS lint
gulp.task("lint:js", function () {
    return gulp.src(srcPath.script + "**/*.js")
        .pipe(jslint())
});

// SAS lint
gulp.task("lint:sass", function () {
    return gulp.src(srcPath.styles + "**/*.scss")
        .pipe(sassLint())
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
});

/*--------------------------------------------------------------
 Watchers
 --------------------------------------------------------------*/
// Watch files
gulp.task("watch", function () {
    gulp.watch(srcPath.html + "**/*.html", ["copy:html"]);
    gulp.watch(srcPath.styles + "**/*.scss", ["copy:styles"]);
    gulp.watch(srcPath.scripts + "**/*.js", ["copy:scripts"]);
});
