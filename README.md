# Jarige job front-end read Me #

### How do I get set up? ###

1. Clone the repository `git clone https://fons-hettema@bitbucket.org/fons-hettema/jarige-job-frontend.git`
2. Run `npm install`
3. Run `gulp` (this will setup an environment at port 3000)
4. Visit http://localhost:3000 in your browser to see the application

### Who do I talk to? ###

E-mail: fons.hettema@incentro.com
