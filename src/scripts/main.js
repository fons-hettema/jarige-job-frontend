// Set namespace
var APP = APP || {};

(function($) {

    // Extend UX
    APP.ux = {

        init: function() {

          if ($(".site").hasClass("page-results")) {
              APP.ux.setTableSlider();
              APP.ux.tableCheckboxes();
          }

          APP.ux.resetSearchForm();
          APP.ux.searchInput();
          APP.ux.showDownloadButton();
          // APP.ux.showFileUploader();

          var inp = $("#file");
          // Dragenter function for styling
          $(".upload__area input").on('dragenter', function(){
            $(".upload__dummy").css({"background-color":"#e5e5e5"});
            $(".upload__dummy").css({"padding-top":"200px"});
          })

          // On drop function for styling
          $(".upload__area input").on('drop', function(){
            $(".upload__dummy").css({"background-color":"#f1f1f1"});
            $(".upload__dummy").css({"padding-top":"175px"});
            $(".upload__dummy").css({"width":"75%"});
            $(".upload__area input").css({"width":"75%"});
            $(".upload__dummy__content").css({"width":"25%"});
            $(".upload__dummy__content").css({"padding":"10px"});
          })

          // Dragleave function for styling
          $(".upload__area input").on('dragleave', function(){
            $(".upload__dummy").css({"background-color":"#f1f1f1"});
            $(".upload__dummy").css({"padding-top":"175px"});
          })

          // Show names of inputfiles
          inp.change(function() {
            var amount = $(this).get(0).files.length;
                for (var i = 0; i < amount; ++i) {
                var name = $(this).get(0).files.item(i).name;
                $(".upload__dummy__content").prepend($("<p><i class='fa fa-file-excel-o'></i>" + " " + name + "<p/>").hide().fadeIn(500));
              }
              $(".upload__dummy").css({"width":"75%"});
              $(".upload__area input").css({"width":"75%"});
              $(".upload__dummy__content").css({"width":"25%"});
              $(".upload__dummy__content").css({"padding":"10px"});
          })
        },

        resetSearchForm: function() {
          $(".search-input").val("");
        },

        searchInput: function() {
          // Search input
          $(".search-input").focusout(function() {
              $(".search__results").fadeOut(100);
          });

          // $(".search-input").keyup(function() {
          //     if ($(this).val() === '') {
          //         $(".search__results").fadeOut(100);
          //     } else {
          //         $(".search__results").fadeIn(100);
          //     }
          // });
        },

        setTableSlider: function() {
          $("#slider-2").slider({
              tooltip: 'always',
              min: 0,
              max: 100,
              value: [75, 90],
              focus: false
          });
          setTimeout(function() {
              $("#slider-2").slider('relayout');
          }, 1000);
        },

        tableCheckboxes: function() {
          // Results
          $("tr").click(function() {
              var that = $(this);
              if ($(this).find("input").attr("checked")) {
                  $(this).find("input").attr("checked", false);
                  that.removeClass("highlighted");
              } else {
                  $(this).find("input").attr("checked", true);
                  that.addClass("highlighted");
              }
          });
        },

        showDownloadButton: function() {
          // Export
          $(".export__form-select").change(function() {
              $(this).closest('.export__item').addClass("active");
          });
        }
    }
})(jQuery);

APP.ux.init();
