// Set namespace
var APP = APP || {};

(function($) {

    APP.secure = {

        init: function() {
          APP.secure.checkSession();
        },

        redirectTo: function(url) {
          window.location = url;
        },

        checkSession: function() {
          var dateNow = new Date();
          var key = sessionStorage.getItem("access_token");
          var expireDate = sessionStorage.getItem("expire_date");
          console.log(expireDate);
          if(!key || expireDate < 60000) {
            sessionStorage.clear();
            if(!$("body").hasClass("page-login")) {
              APP.secure.redirectTo('/login.html');
            }
          }
        }
    }
})(jQuery);

APP.secure.init();
