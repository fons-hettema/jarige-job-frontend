 function start() {
      gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
          client_id: '198241906738-b72uqgjv11i7oe1ob9bph4likesmd3pn.apps.googleusercontent.com',
          // Scopes to request in addition to 'profile' and 'email'
          //scope: 'additional_scope'
        });
      });
    }

function clickBut(){
    auth2.grantOfflineAccess().then(signInCallback);
}

 function signInCallback(authResult) {
    if (authResult['code']) {

      // Hide the sign-in button now that the user is authorized, for example:
      $('#signinButton').attr('style', 'display: none');

      // Send the code to the server
      $.ajax({
        type: 'POST',
        url: 'http://example.com/storeauthcode',
        // Always include an `X-Requested-With` header in every AJAX request,
        // to protect against CSRF attacks.
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        contentType: 'application/octet-stream; charset=utf-8',
        success: function(result) {
          // Handle or verify the server response.
        },
        processData: false,
        data: authResult['code']
      });
    } else {
      // There was an error.
    }
  }